package com.company;

public class Main {
    // 1, 2, 4, 7
    public static void main(String[] args) {
	      FindFile api = new FindFile();

        api.searchExt("XML").forEach((item) -> {
            System.out.println(item.getName());
        });
    }
}

// find files greater than size N

// find files with names that contain ("." + fileExt) recursive file search

// Things we will need
    // Class File
        // contains metadata for a single file including name, size, possibly URI
    // Class FileSystem (is this needed?)
        // manages vector of file objects
        // FindFile API contains an instance of this? pretty sure that's bad object oriented design