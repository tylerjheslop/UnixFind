package com.company;

import java.util.Iterator;
import java.util.Vector;

public class FindFile {
  Vector<File> index;

  public FindFile() {
    // create a file system index using java's file class
    // iterate through computer's filesystem
    // add all files in each directory
  }

  //search for file name
  public Vector<File> search(String filename) {
    Vector<File> searchResults = new Vector<File>();
    Iterator<File> it = index.iterator();

    while (it.hasNext()) {
      File current = it.next();
      if (current.getName() == filename) {
        searchResults.add(current);
      }
    }

    return searchResults;
  }

  /**
   * @param size    ---> size of the files
   * @param options ---> 0 for (<), 1 for (>), 2 for (==)
   * @return vector of files
   */
  public Vector<File> searchSize(double size, int options) {
    Vector<File> searchResults = new Vector<File>();
    Iterator<File> it = index.iterator();

    while (it.hasNext()) {
      File current = it.next();
      if (options == 0) {
        if (current.getSize() < size) {
          searchResults.add(current);
        }
      } else if (options == 1) {
        if (current.getSize() > size) {
          searchResults.add(current);
        }
      } else {
        if (current.getSize() == size) {
          searchResults.add(current);
        }
      }
    }
    return searchResults;
  }

  //search for ext in name
  public Vector<File> searchExt(String ext) {
    Vector<File> searchResults = new Vector<File>();
    Iterator<File> it = index.iterator();

    while (it.hasNext()) {
      File current = it.next();

      if (current.getExt() == ext) {
        searchResults.add(current);
      }
    }

    return searchResults;
  }
}
