package com.company;
// Class File
// contains metadata for a single file including name, size, possibly URI
public class File {
  String name;
  String uri;
  String ext;
  double size;  // is this proper?

  public File(String name, String uri, double size) {
    this.name = name;
    this.uri = uri;
    setExt();
    this.size = size;
  }

  private void setExt() {
    if (this.name.contains(".")) {
      this.ext = this.name.substring(this.name.indexOf("."), this.name.length() -1);
    }
  }
  public String getName() {
    return name;
  }

  public String getUri(){
    return uri;
  }

  public double getSize(){
    return size;
  }

  public String getExt() {
    return ext;
  }
}

